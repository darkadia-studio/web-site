<?php
	header('content-type: text/css');
	ob_start('ob_gzhandler');
	header('Cache-Control: max-age=0, must-revalidate');
	// etc. 
?>

body{
	padding-top:70px;
}
.affix {
      top: 0;
      width: 100%;
	z-index: 100;
}
.navbar-right{
	padding-right: 15px;
}
.inverse-dropdown {
  background-color: #222;
  border-color: #080808;
}
.inverse-dropdown > li > a {
  color: #999;
}
.inverse-dropdown > li > a:hover {
  color: #fff;
  background-color: #000;
}
.inverse-dropdown > .divider {
  background-color: #000;
}
