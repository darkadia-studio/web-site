<?php require('../include/httpsRedirect.php');?>
<!DOCTYPE html>
<html>
	<?php
		$pageName='Pictures';
		require('../include/data.php');
		require('../include/head.php');
	?>
	
	<body>
		<?php
			require('../include/header.php');
			require('../include/nav.php');
		?>
		
		<?php
			require('../include/footer.php');
		?>
	</body>
</html>
