<?php 
	$meta = 'home-meta';
	require('include/data.php');
?>

<?php
	$sections = $bdd->selectSpecialReq('SELECT `titleTag`,`idTag`,`classTag`,`code`,`neededAuth` FROM `index_section` ORDER BY `orderList` ASC');
	foreach($sections as $section){
		if(authorisation::IsEnoughAuth($section['neededAuth'], $auth)){
		?>
		<fieldset class="<?php echo $section['classTag'];?>" id="<?php echo $section['idTag'];?>">
			<legend class="h2"><?php
				echo $pageTexts[$section['titleTag']];
			?></legend>
			<div><?php
				eval($section['code']);
			?></div>
		</fieldset>
		<p class="spacer"></p>
			<?php
			}
		}
		
	?>
	<?php
		require('include/footer.php');
	?>
</body>
</html>