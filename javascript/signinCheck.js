var tokened = false;
//var usernameRegex = "/^[[:alnum:]\p{Common}\p{Arabic}\p{Armenian}\p{Bengali}\p{Bopomofo}\p{Braille}\p{Buhid}\p{Canadian_Aboriginal}\p{Cherokee}\p{Cyrillic}\p{Devanagari}\p{Ethiopic}\p{Georgian}\p{Greek}\p{Gujarati}\p{Gurmukhi}\p{Han}\p{Hangul}\p{Hanunoo}\p{Hebrew}\p{Hiragana}\p{Inherited}\p{Kannada}\p{Katakana}\p{Khmer}\p{Lao}\p{Latin}\p{Limbu}\p{Malayalam}\p{Mongolian}\p{Myanmar}\p{Ogham}\p{Oriya}\p{Runic}\p{Sinhala}\p{Syriac}\p{Tagalog}\p{Tagbanwa}\p{Tai_Le}\p{Tamil}\p{Telugu}\p{Thaana}\p{Thai}\p{Tibetan}\p{Yi}]+$/";
const usernameRegex = /^[a-zA-Z0-9]+$/i;
const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
const passwordRegex = /^[A-Z0-9!@#$%^&*]+$/i;
const nameRegex = /^[A-Z]+$/i;
const tokenRegex = /[A-FX0-9]{8}-[A-FX0-9]{8}-[A-F0-9]{2}/;
var error = '';
var scriptTexts;

function initialiseValues(scriptText) {
    scriptTexts = scriptText;
    var initTag = document.getElementById('initScript');
    initTag.parentNode.removeChild(initTag);
    document.getElementById('submit').disabled = true;
}

function showHelp(input) {
    var helpSpan = document.getElementById(input + 'Help');
    helpSpan.innerHTML = scriptTexts[input + 'Help'];
}

function hideHelp(input) {
    var helpSpan = document.getElementById(input + 'Help');
    helpSpan.innerHTML = "";
    checkTimer(input, 0);
}

function check(input) {
    checkInput(input, 800);
}

function checkTimer(input, timer) {
    checkInput(input, timer);
}

var x_timer = {
    "username": 0, "email": 0, "emailCheck": 0,
    "password": 0, "passwordCheck": 0, "lastname": 0,
    "firstname": 0, "birthday": 0, "token": 0
};
function checkInput(input, timer) {
    clearTimeout(x_timer[input]);
    x_timer[input] = setTimeout(function () {
        checkValidity(input);
    }, timer);
}

function checkValidity(input) {
    var inputValue
    if (input != "null") {
        inputValue = document.getElementById(input).value;
    }

    switch (input) {
        case 'username':
            verifyUsernameValidity(inputValue);
            break;
        case 'email':
            verifyEmailValidity(inputValue);
            break;
        case 'emailCheck':
            verifyEmailCheckValidity(inputValue);
            break;
        case 'password':
            verifyPasswordValidity(inputValue);
            break;
        case 'passwordCheck':
            verifyPasswordCheckValidity(inputValue);
            break;
        case 'lastname':
            verifyNameValidity(input, inputValue);
            break;
        case 'firstname':
            verifyNameValidity(input, inputValue);
            break;
        case 'birthday':
            verifyBirthdayValidity(inputValue);
            break;
        case 'token':
            verifyTokenValidity(inputValue);
            break;
        default:
            break;
    }

    checkCanSubmit();

}

function checkCanSubmit() {
    var usernameInput = document.getElementById('msgBoxUsername');
    var emailInput = document.getElementById('msgBoxEmail');
    var emailCheckInput = document.getElementById('msgBoxEmailCheck');
    var passwordInput = document.getElementById('msgBoxPassword');
    var passwordCheckInput = document.getElementById('msgBoxPasswordCheck');
    var tokenInput;
    if (document.getElementById('token')) {
        tokenInput = document.getElementById('msgBoxToken');
    } else {
        tokenInput = null;
    }
    if (usernameInput.hasClass('alert-success') &
        emailInput.hasClass('alert-success') & emailCheckInput.hasClass('alert-success') &
        passwordInput.hasClass('alert-success') & passwordCheckInput.hasClass('alert-success')) {
        if (tokenInput == null || tokenInput.hasClass('alert-success')) {
            document.getElementById('submit').disabled = false;
        } else {
            document.getElementById('submit').disabled = true;
        }
    } else {
        document.getElementById('submit').disabled = true;
    }
}

function verifyUsernameValidity(inputValue) {
    /**/
    var msgBoxUsername = document.getElementById("msgBoxUsername");
    var warningBox = document.getElementById("warningBox");

    /**/
    if (inputValue.length == 0) {
        msgBoxUsername.className = "col-md-4 alert alert-danger";
        msgBoxUsername.innerHTML = scriptTexts['noUsernameError'];
    } else if (inputValue.length < 2) {
        msgBoxUsername.className = "col-md-4 alert alert-danger";
        msgBoxUsername.innerHTML = scriptTexts['tooShortUsernameError'];
    } else if (inputValue.length > 30) {
        msgBoxUsername.className = "col-md-4 alert alert-danger";
        msgBoxUsername.innerHTML = scriptTexts['tooLongUsernameError'];
    } else if (!usernameRegex.exec(inputValue)) {
        msgBoxUsername.className = "col-md-4 alert alert-danger";
        msgBoxUsername.innerHTML = scriptTexts['invalidusernameRegexacter'];
    } else {
        $.post("/external/existingValue.php", { valueToSearch: "reservedUsername", value: inputValue }, function (data) {
            if (data == 1) {
                msgBoxUsername.className = "col-md-4 alert alert-warning";
                msgBoxUsername.innerHTML = scriptTexts['reservedUsername'];
                if (!document.getElementById("tokenInput") && !!document.getElementsByClassName("tokenButton")) {
                    addTokenBtn();
                }
                if(document.getElementById('token')){
                    checkTimer('token', 0);
                }
            } else {
                removeTokenBtn();
                var tokenInput;
                if (document.getElementById("tokenInput")) {
                    tokenInput = document.getElementById("tokenInput");
                }
                if (tokenInput != null) {
                    tokenInput.remove();
                }
                $.post("/external/existingValue.php", { valueToSearch: "existingUsername", value: inputValue }, function (data) {
                    if (data == 1) {
                        msgBoxUsername.className = "col-md-4 alert alert-warning";
                        msgBoxUsername.innerHTML = scriptTexts['existingUsername'];
                        var existingAccount = scriptTexts['existingUsernameAccount'];
                        var lostPass = scriptTexts['lostPassword'];
                        if (!document.getElementsByClassName("accountButton")) {
                            var lostPassBtn;
                            if (!document.getElementById("lostPassBtn")) {
                                lostPassBtn = document.createElement("button");
                                lostPassBtn.setAttribute("id", "lostPassBtn");
                                lostPassBtn.setAttribute("class", "accountButton");
                                lostPassBtn.setAttribute("type", "button");
                                lostPassBtn.addEventListener("click", goLostPassword);
                                lostPassBtn.innerHTML = existingAccount;
                            } else {
                                lostPassBtn = document.getElementById("lostPassBtn");
                            }
                            var loginBtn;
                            if (!document.getElementById("loginBtn")) {
                                loginBtn = document.createElement("button");
                                loginBtn.setAttribute("id", "loginBtn");
                                loginBtn.setAttribute("class", "accountButton");
                                loginBtn.setAttribute("type", "button");
                                loginBtn.addEventListener("click", goLogin);
                                loginBtn.innerHTML = lostPass;
                            } else {
                                loginBtn = document.getElementById("loginBtn");
                            }
                            warningBox.appendChild(lostPassBtn);
                            warningBox.appendChild(loginBtn);
                        }
                    } else {
                        msgBoxUsername.className = "col-md-4 alert alert-success";
                        msgBoxUsername.innerHTML = scriptTexts['validUsername'];
                        checkTimer("null", 0);
                    }
                });
            }
        });
    }
}

function verifyEmailValidity(inputValue) {
    var msgBoxEmail = document.getElementById("msgBoxEmail");

    var existingAccount = scriptTexts['existingUsernameAccount'];
    var lostPass = scriptTexts['lostPassword'];
    if (inputValue.length <= 0) {
        msgBoxEmail.className = "col-md-4 alert alert-danger";
        msgBoxEmail.innerHTML = scriptTexts['noEmailError'];
    } else if (!emailRegex.exec(inputValue)) {
        msgBoxEmail.className = "col-md-4 alert alert-danger";
        msgBoxEmail.innerHTML = scriptTexts['invalidEmailError'];
    } else {
        $.post("/external/existingValue.php", { valueToSearch: "existingEmail", value: inputValue }, function (data) {
            if (data == 1) {
                msgBoxEmail.className = "col-md-4 alert alert-warning";
                msgBoxEmail.innerHTML = scriptTexts['existingEmail'];
                var loginBtn;
                if (!document.getElementById("loginBtn")) {
                    loginBtn = document.createElement("button");
                    loginBtn.setAttribute("id", "loginBtn");
                    loginBtn.setAttribute("class", "accountButton");
                    loginBtn.setAttribute("type", "button");
                    loginBtn.addEventListener("click", goLogin);
                    loginBtn.innerHTML = lostPass;
                } else {
                    loginBtn = document.getElementById("loginBtn");
                }
                var lostPassBtn;
                if (!document.getElementById("lostPassBtn")) {
                    lostPassBtn = document.createElement("button");
                    lostPassBtn.setAttribute("id", "lostPassBtn");
                    lostPassBtn.setAttribute("class", "accountButton");
                    lostPassBtn.setAttribute("type", "button");
                    lostPassBtn.addEventListener("click", goLostPassword);
                    lostPassBtn.innerHTML = existingAccount;
                } else {
                    lostPassBtn = document.getElementById("lostPassBtn");
                }
                if (!document.getElementsByClassName("accountButton")) {
                    warningBox.appendChild(lostPassBtn);
                    warningBox.appendChild(loginBtn);
                }
            } else {
                msgBoxEmail.className = "col-md-4 alert alert-success";
                msgBoxEmail.innerHTML = scriptTexts['validEmail'];
                if (document.getElementById("tokenInput")) {
                    checkTimer('token', 0);
                }
                checkTimer("null", 0);
            }
        });
    }
}

function verifyEmailCheckValidity(inputValue) {
    var mailValue = document.getElementById('email').value;
    var msgBoxEmail = document.getElementById("msgBoxEmailCheck");
    if (!document.getElementById('msgBoxEmail').hasClass('alert-success')) {
        msgBoxEmail.className = "col-md-4 alert alert-danger";
        msgBoxEmail.innerHTML = scriptTexts['invalidEmailCheck'];
    } else if (inputValue.toLowerCase() != mailValue.toLowerCase()) {
        msgBoxEmail.className = "col-md-4 alert alert-danger";
        msgBoxEmail.innerHTML = scriptTexts['noSameEMailError'];
    } else {
        msgBoxEmail.className = "col-md-4 alert alert-success";
        msgBoxEmail.innerHTML = scriptTexts['validEmailCheck'];
    }
}

function verifyPasswordValidity(inputValue) {
    var msgBoxPassword = document.getElementById("msgBoxPassword");

    if (inputValue.length <= 0) {
        msgBoxPassword.className = "col-md-4 alert alert-danger";
        msgBoxPassword.innerHTML = scriptTexts['noPasswordError'];
    } else if (inputValue.length < 5) {
        msgBoxPassword.className = "col-md-4 alert alert-danger";
        msgBoxPassword.innerHTML = scriptTexts['tooShortPassword'];
    } else if (inputValue.length > 20) {
        msgBoxPassword.className = "col-md-4 alert alert-danger";
        msgBoxPassword.innerHTML = scriptTexts['tooLongPassword'];
    } else if (!passwordRegex.exec(inputValue)) {
        msgBoxPassword.className = "col-md-4 alert alert-danger";
        msgBoxPassword.innerHTML = scriptTexts['invalidPasswordError'];
    } else {
        msgBoxPassword.className = "col-md-4 alert alert-success";
        msgBoxPassword.innerHTML = scriptTexts['validPassword'];
    }
}

function verifyPasswordCheckValidity(inputValue) {
    var passwordValue = document.getElementById('password').value;
    var msgBoxPasswordCheck = document.getElementById("msgBoxPasswordCheck");
    if (!document.getElementById('msgBoxPassword').hasClass('alert-success')) {
        msgBoxPasswordCheck.className = "col-md-4 alert alert-danger";
        msgBoxPasswordCheck.innerHTML = scriptTexts['invalidPasswordCheck'];
    } else if (inputValue != passwordValue) {
        msgBoxPasswordCheck.className = "col-md-4 alert alert-danger";
        msgBoxPasswordCheck.innerHTML = scriptTexts['noSamePasswordError'];
    } else {
        msgBoxPasswordCheck.className = "col-md-4 alert alert-success";
        msgBoxPasswordCheck.innerHTML = scriptTexts['validPasswordCheck'];
    }
}

function verifyNameValidity(input, inputValue) {
        var msgBoxName = document.getElementById("msgBox" + input.capitalizeFirstLetter());
    if (inputValue.length > 0) {
        if (!nameRegex.exec(inputValue)) {
            msgBoxName.className = "col-md-4 alert alert-danger";
            msgBoxName.innerHTML = scriptTexts['invalid' + input.capitalizeFirstLetter()];
        } else {
            msgBoxName.className = "col-md-4 alert alert-success";
            msgBoxName.innerHTML = scriptTexts['valid' + input.capitalizeFirstLetter()];
        }
    } else {
        msgBoxName.className = "";
        msgBoxName.innerHTML = "";
    }
}

function verifyBirthdayValidity(inputValue) {
    if ('') {
        //Date de naissance invalide
    } else {
        //Date de naissance valide
    }
}

function verifyTokenValidity(inputValue) {
    var username = document.getElementById('username').value;
    var email = document.getElementById('email').value;
    var msgBoxToken = document.getElementById("msgBoxToken");
    if (inputValue.length <= 0) {
        msgBoxToken.className = "col-md-4 alert alert-danger";
        msgBoxToken.innerHTML = scriptTexts['noTokenError'];
    } else if (!tokenRegex.exec(inputValue)) {
        msgBoxToken.className = "col-md-4 alert alert-danger";
        msgBoxToken.innerHTML = scriptTexts['invalidTokenError'];
    } else if (!document.getElementById("msgBoxEmail").hasClass('alert-success')) {
        msgBoxToken.className = "col-md-4 alert alert-danger";
        msgBoxToken.innerHTML = scriptTexts['noEmailTokenInput'];
    } else {
        $.post("external/existingValue.php", { valueToSearch: "token", token: inputValue, username: username, email: email }, function (data) {
            switch (parseInt(data)) {
                case 0:
                    msgBoxToken.className = "col-md-4 alert alert-danger";
                    msgBoxToken.innerHTML = scriptTexts['innexistantTokenStorage'];
                    break;
                case -1:
                    msgBoxToken.className = "col-md-4 alert alert-danger";
                    msgBoxToken.innerHTML = scriptTexts['invalidEmailTokenInput'];
                    break;
                case -2:
                    msgBoxToken.className = "col-md-4 alert alert-danger";
                    msgBoxToken.innerHTML = scriptTexts['invalidTokenInput'];
                    break;
                case 1:
                    msgBoxToken.className = "col-md-4 alert alert-success";
                    msgBoxToken.innerHTML = scriptTexts['validToken'];
                    document.getElementById('msgBoxUsername').setAttribute("class", "alert-success");
                    document.getElementById('msgBoxUsername').innerHTML = scriptTexts['validUsername'];
                    checkTimer("null", 0);
                    break;
            }
        });
    }


}

function goLogin(data) {

}

function goLostPassword(data) {

}

function addTokenInput() {

    document.getElementById("tokenBtns").remove();
    var tokenDivGroup = document.getElementById("forToken");
    tokenDivGroup.innerHTML = "<label class=\"col-md-4 control-label\" for=\"token\">"+scriptText['token']+" :</label>"+"\r\n"+  
    "<div class=\"col-md-4\">"+"\r\n"+  
    "<input onkeyup=\"check('token')\" onfocus=\"showHelp('token')\" onblur=\"hideHelp('token')\"  id=\"token\" name=\"token\" type=\"text\" placeholder=\""+scriptText['tokenPlaceholder']+"\" class=\"form-control input-md\" required=\"\">"+"\r\n"+  
    "<span class=\"help-block\" id=\"tokenHelp\"></span>"+"\r\n"+  
    "</div>"+"\r\n"+  
    "<div class=\"col-md-4\" id=\"msgBoxToken\"></div>"
}

function addTokenBtn(){
    document.getElementById("tokenBtns").remove();
    var tokenBtnDivGroup = document.getElementById("usernameExistence");
    tokenBtnDivGroup.innerHTML = "<div class=\"col-md-12 btn-group\" id=\"tokenBtns\" role=\"group\" >"+"\r\n"+ 
    "<button type=\"button\"  id=\"tokenBtn\" name=\"tokenBtn\" class=\"btn btn-info col-md-6\">"+scriptTexts['haveAToken']+"</button>"+"\r\n"+
    "<button type=\"button\"  id=\"contactBtn\" name=\"contact\" class=\"btn btn-info col-md-6\">"+scriptText['myUsername']+"</button>"+"\r\n"+
    "</div>"
}

function contactSupport() {

}

String.prototype.capitalizeFirstLetter = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

Element.prototype.hasClass = function (cls) {
    return (' ' + this.className + ' ').indexOf(' ' + cls + ' ') > -1;
}
/*

    <button type="submit"  id="submit" name="submit" class="btn btn-default"><?php echo $pageTexts['confirm']?></button>
    <button type="reset"  id="reset" name="reset" class="btn btn-inverse"><?php echo $pageTexts['reset']?></button>

    */