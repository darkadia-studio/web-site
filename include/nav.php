<?php
	function getRightUrl($link,$userLang){
		$finalLink = $link;
		$tempLink='';		
		if(substr($link, 0,4)==='http'){
			$tempLink = $link;
		} else {
			$link = $userLang.'/'.$link;
				$tempLink = '/'.$link;
		}
		$finalLink = $tempLink;
		return $finalLink;
	}
	
	function setLangLink($userLang, $targetLang){
		$actualLink;
		if($targetLang == null){
			$targetLang = $userLang;
		}
		$finalLink = '';
		$tempLink='';
			$actualLink = $_SERVER['REQUEST_URI'];
			if(strstr($actualLink, $userLang)){
				$tempLink = str_replace('/'.$userLang.'/','/'.$targetLang.'/',$actualLink);
			} else {
				$tempLink = '/'.$targetLang.'/'.$actualLink;
			}
		$finalLink = $tempLink;
		while(strstr($finalLink, '//')){
			$finalLink = str_replace('//','/',$finalLink);
		}
		return $finalLink;
	}
?>

<nav class="navbar navbar-inverse navbar-static-top" data-spy="affix" data-offset-top="150">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
  <ul class="nav navbar-nav">
<?php
			$navigations = $bdd->selectSpecialReq('SELECT `navigation`.`id`, `navigation`.`name` AS pageName, `navigation`.`url`, `navigation`.`image`, `navigation`.`noFollow`,`navigation`.`type`,`navigation`.`neededAuth`, `navigation`.`hasSub`, `link-navigation_lang`.`name`, `link-navigation_lang`.`description`'.
				' FROM `navigation`'.
				' JOIN `link-navigation_lang`'.
				' ON `navigation`.`id` = `link-navigation_lang`.`id_navigation`'.
				' JOIN `lang`'.
				' ON `link-navigation_lang`.`id_lang` = `lang`.`id`'.
				' WHERE `navigation`.`isSub` = 0 AND (`navigation`.`type` = 2 OR `navigation`.`type`= '.$connected.') AND `lang`.`letters` = \''.$userLang.'\''.
				' ORDER BY `navigation`.`orderList` ASC');
			foreach($navigations as $navigation){
				if(authorisation::IsEnoughAuth($navigation['neededAuth'], $auth)){
					?>
		<li class="<?php if($navigation['hasSub']){?>dropdown <?php } if($pageName==$navigation['pageName']){?>active<?php }?>">
			<a title="<?php echo $navigation['description'];?>" href="<?php echo getRightUrl($navigation['url'],$userLang);?>"<?php if($navigation['noFollow']){ ?>rel="nofollow"<?php } ?>><?php echo $navigation['name']; ?></a>
						<?php
						if($navigation['hasSub']){
							$subNavigations = $bdd->selectSpecialReq('SELECT `navigation`.`url`, `navigation`.`image`, `navigation`.`noFollow`,`navigation`.`type`,`navigation`.`neededAuth`, `navigation`.`hasSub`, `link-navigation_lang`.`name`, `link-navigation_lang`.`description`'.
								' FROM `navigation`'.
								' JOIN `link-navigation_lang`'.
								' ON `navigation`.`id` = `link-navigation_lang`.`id_navigation`'.
								' JOIN `lang`'.
								' ON `link-navigation_lang`.`id_lang` = `lang`.`id`'.
								' WHERE `navigation`.`isSub` = 1 AND (`navigation`.`type` = 2 OR `navigation`.`type`= '.$connected.') AND `lang`.`letters` = \''.$userLang.'\' AND `navigation`.`id` = '.$navigation['id'].
								' ORDER BY `navigation`.`orderList` ASC');
							foreach($subNavigations as $subNavigation){
								if(authorisation::IsEnoughAuth($subNavigation['neededAuth'], $auth)){
									?>
									<a title="<?php echo $subNavigation['description'];?>" href="<?php echo getRightUrl($subNavigation['url'], $userLang);?>"<?php if($subNavigation['noFollow']){?> rel="nofollow" <?php } ?>><?php echo $subnavigation['name'];?></a>
									<?php
								}
							}
						}
						?>
						</li>
						<?php
				}
			}
			?>
</ul>
<ul class="nav navbar-nav navbar-right">

	<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="/img/blank.gif" class="flag flag-<?php echo $langFlag;?>" alt="<?php echo $userLang;?> flag"> <?php echo $langName;?><span class="caret"></span></a>
              <ul class="dropdown-menu inverse-dropdown">
			<?php
			$langsLink = $bdd->select(['letters','flag', 'fullname'], 'lang');
			foreach($langsLink as $langLink){
				if($langLink['letters']!=$userLang){
					?>
                <li><a href="<?php echo setLangLink($userLang, $langLink['letters']);?>"><img src="/img/blank.gif" class="flag flag-<?php echo $langLink['flag'];?>" alt="<?php echo $langLink['letters'];?> flag"> <?php echo $langLink['fullname'];?></a></li>
					<?php
				}
			}
		?>
              </ul>
            </li>
  </ul>
        </div>
</nav>
