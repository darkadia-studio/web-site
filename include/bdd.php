<?php

class bdd{
		CONST host = 'host';
		CONST user = 'user';
		CONST pass = 'password';
		CONST base = 'database';
		
		private $db;

	public function connectDB(){
		try{
			$this->db = new PDO('mysql:host='.$this::host.';dbname='.$this::base, $this::user, $this::pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')) or die('Impossible de se connecter � la base de donn�e');
		} catch (Exception $excep){
			die($excep);
		}
	}

	public function closeDB(){
		mysql_close();
	}

	public function select($selected, $table, $ordered = false, $orderedBy = 'id', $order = 'ASC'){
		$request = 'SELECT ';
		if(is_array($selected)){
			foreach($selected as $id => $selectedVal){
				$request .= '`'.$selectedVal.'`';
				if(isset($selected[$id+1])){
					$request .= ', ';
				}
			}
		} else {
			if($selected=='*'){
				$request .= $selected;
			} else {
				$request .= '`'.$selected.'`';
			}
		}
		$request .= ' FROM `'.$table.'`';
		if($ordered){
			$request.=' ORDER BY '.$orderedBy.' '.$order;
		}

		$select = $this->db->prepare($request);
		$select->execute();
		$results = $select->fetchAll();
		return $results;
	}
	
	public function selectByValue($selected, $table, $indexes, $values, $ordered = false, $orderedBy = 'id', $order = 'ASC'){
		$request = 'SELECT ';
		if(is_array($selected)){
			foreach($selected as $id => $selectedVal){
				$request .= '`'.$selectedVal.'`';
				if(isset($selected[$id+1])){
					$request .= ', ';
				}
			}
		} else {
			if($selected=='*'){
				$request .= $selected;
			} else {
				$request .= '`'.$selected.'`';
			}
		}
		$request .= ' FROM `'.$table.'`';
		$request .= ' WHERE `';
		if(is_array($indexes)){
			foreach($indexes as $id => $index){
				$request .= $index.'`';
				
				if(is_array($values[$id])){
					foreach($values[$id] as $idV => $value){
						if(strstr($values[$id][$idV],'%')){
							$request .= ' LIKE ';
						} else {
							$request .= ' = ';
						}
						$request .= '\''.strval($value).'\'';
						if(isset($values[$id][$idV+1])){
							$request .= ' OR `';							
							$request .= $index.'`';
						}
					}
				} else {
					if(strstr($values[$id],'%')){
						$request .= ' LIKE ';
					} else {
						$request .= ' = ';
					}
					$request .= '\''.strval($values[$id]).'\'';
				}
				if(isset($values[$id+1])){
					$request .= ' AND `';
				}
			}
		} else {
			$request .= $indexes.'` = \''.strval($values).'\'';
		}
		if($ordered){
			$request .= ' ORDER BY `'.$orderedBy.'` '.$order;
		}

		$select = $this->db->prepare($request);
		$select->execute();
		$results = $select->fetchAll();
		return $results;
	}
	
	public function selectSpecialReq($request){
		$select = $this->db->prepare($request);
		$select->execute();
		$results = $select->fetchAll();
		return $results;
	}

	public function insert($table, array $colNames, array $colValues){
		$request = 'INSERT INTO '.$table.'(';
		$i = 0;
		foreach($colNames as $colName){
			$request += $colName;
			if(++$i < count($colNames)){
				$request += ',';
			}
		}
		$request += ') VALUES(';
		
		$i = 0;
		foreach($colValues as $colValue){
			$request += $colValue;
			if(++$i < count($colValues)){
				$request += ',';
			}
		}
		$request += ')';
		$result = mysql_query($resquest) or die('Erreur SQL !<br>'.$request.'<br>'.mysql_error());
		return $result;
	}

	public function update(){
		echo 'Fonction update non impl�ment�';
	}

	public function delete(){
		echo 'Fonction delete non impl�ment�';
	}
/*
// on fait une boucle qui va faire un tour pour chaque enregistrement
while($data = mysql_fetch_assoc($req))
    {
    // on affiche les informations de l'enregistrement en cours 
    echo $data['IP'].';'.$data['Port'].';'.$data['Name'].';'.$data['Statut'].';'.$data['Mode'].';'.$data['MaxPlayers'].'<br>';
    }*/
	
}

?>