<?php

class facebook{
	
	CONST appID = 1753113188299788;

	public function meta(){
		return '<meta property="fb:app_id" content="'.$this::appID.'" />';
	}
	
	public function script($lang){
		echo "<script>";
		echo "window.fbAsyncInit = function()";
		echo "{ FB.init({ appId:'".$this::appID."',xfbml:true,version:'v2.7'});};";
		echo "(function(d, s, id){";
		echo "var js, fjs = d.getElementsByTagName(s)[0];";
		echo "if (d.getElementById(id)) {return;}js = d.createElement(s); js.id = id;";
		echo "js.src = \"//connect.facebook.net/".$lang."/sdk.js#xfbml=1&version=v2.7&appId=".$this::appID."\";fjs.parentNode.insertBefore(js, fjs);";
		echo "}(document, 'script', 'facebook-jssdk'));";
		echo "</script>";
	}
	
	public function comment($mobile){
		echo "<div class=\"fb-comments\" data-href=\"https://www.evolvingworld.darkadia-studio.com\" data-numposts=\"3\" data-mobile=\"".strval($mobile)."\" ></div><br>";
	}
	
	public function likePage($mobile){
		echo "<div class=\"fb-like\" data-href=\"https://www.evolving-world.darkadia-studio.com\" data-mobile=\"".strval($mobile)."\" data-layout=\"standard\" data-action=\"like\" data-size=\"small\" data-show-faces=\"true\" data-share=\"true\"></div><br>";
	}
	
	public function likeFbPage($mobile){
		echo "<div class=\"fb-like\" data-href=\"https://www.facebook.com/EvolvingWorldTheGame/\" data-mobile=\"".strval($mobile)."\" data-layout=\"standard\" data-action=\"like\" data-size=\"small\" data-show-faces=\"true\" data-share=\"true\"></div>";
	}

}

?>