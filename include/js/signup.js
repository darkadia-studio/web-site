var usernameCar = "/^[0-9a-zA-Z]+$/";
var nameCar = "/^[a-zA-Z]+$/"
var error = '';
var existingUsername = ['Test', 'Try it'];
var reservedUsername = ['Jean', 'Do it'];
var existingEmail;
var pageTexts;

function showHelp(input){
  	var helpSpan = document.createElement('span');
    helpSpan.setAttribute('id', input+'Help');
    helpSpan.setAttribute('class', 'helpTooltip');
    var tooltipText = document.createTextNode(input+'Help');
    helpSpan.appendChild(tooltipText);
    var parentTag = document.getElementById(input+'Tooltip');
    parentTag.appendChild(helpSpan);
	
	var errorSpan = document.getElementById(input+'Error');
	errorSpan.parentNode.removeChild(errorSpan);
}

function hideHelp(input){
	var helpSpan = document.getElementById(input+'Help');
	helpSpan.parentNode.removeChild(helpSpan);
	checkValidity(input);
}

function checkValidity(input){
	var errorSpan = document.createElement('span');
    errorSpan.setAttribute('id', input+'Error');
    errorSpan.setAttribute('class', 'errorTooltip');
	
	var inputTag = document.getElementById(input);
	
	switch(input){
		case 'username':
			if(existingUsername.includes(inputTag.value)){
				error = 'ExistingUsernameError';
			} else if(reservedUsername.includes(inputTag.value)){
				error = 'ReservedUsernameError';
			} else if(inputTag.value.length == 0){
				error = 'NoUsernameError';
			} else if (inputTag.value.length < 2) {
				error = 'TooShortUsername';
			} else if (inputTag.value.length > 30) {
				error = 'TooLongUsername';
			} else if(!usernameCar.test(inputTag.value)){
				error = 'InvalidUsernameCaracter'
			}
		break;
		case 'email':
		break;
		case 'emailCheck':
		break;
		case 'password':
		break;
		case 'passwordCheck':
		break;
		case 'lastname':
		break;
		case 'firstname':
		break;
		case 'birthday':
		break;
	}
	
	if(error.length!=-1){
    	var errorText = document.createTextNode(error);
    	errorSpan.appendChild(errorText);
    	var parentTag = document.getElementById(input+'Tooltip');
		parentTag.appendChild(errorSpan);
	}
	error = '';
}

function initialiseValues(){
	var initTag = document.getElementById('initScript');
	initTag.parentNode.removeChild(initTag);
	alert("Init Done");
}
                                     