<?php
	//require('httpsRedirect.php');
	
	require('bdd.php');
	require('authorisation.php');
	require('facebook/facebook.php');
	require('bbcode.php');
	require('linkUti.php');
	require('utilities.php');
	
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= filemtime($localFileName)){
		header('HTTP/1.0 304 Not Modified');
		exit;
	}
	$userLang = 'en';
	$siteLang;
	$langFlag;
	$connected = 0;
	$auth = 1024;
	$linkPref = '';
	$mobile;
	$date = new DateTime();
	require('mobileDetect.php');
	
	$detect = new Mobile_Detect();
	
	if ($detect->isMobile()){
		$mobile = 'true';
		} else {
		$mobile = 'false';
	}
	
	if(isset($_GET['userLang'])){
		if($_GET['userLang']!=''){
			$userLang = $_GET['userLang'];
		}
		} else if(isset($_SERVER['REDIRECT_QUERY_STRING'])){
		if(strstr($_SERVER['REDIRECT_QUERY_STRING'],'userLang')){
			$userLang = substr($_SERVER['REDIRECT_QUERY_STRING'],strpos($_SERVER['REDIRECT_QUERY_STRING'],'userLang=')+9,2);
		}
	}
	
$bdd = new bdd;
$bdd->connectDB();
$facebook = new facebook;
$bbcode = new bbcode;

$langs = $bdd->selectByValue(['letters', 'acronym', 'flag', 'fullname'], 'lang', 'letters', $userLang);
foreach($langs as $lang){
$userLang = $lang['letters'];
$siteLang = $lang['acronym'];
$langFlag = $lang['flag'];
$langName = $lang['fullname'];
}

ob_start("ob_gzhandler");

$pageName=str_replace('.php','',$_SERVER['PHP_SELF']);
$pageName = substr($pageName,strrpos($pageName,'/')+1);

$pageTitleName = $pageName;
$pageTexts = array();
$texts = $bdd->selectSpecialReq('SELECT `text`.`name`, `text`.`trad`'.
' FROM `text` JOIN `lang` ON `text`.`id_lang` = `lang`.`id`'.
' WHERE (`text`.`pages` LIKE \'%'.$pageName.'%\' OR `text`.`pages` = \'all\') AND `lang`.`letters` = \''.$userLang.'\'');
foreach($texts as $text){
$pageTexts[$text['name']] = $text['trad'];
}

if(isset($pageTexts[$pageName.'-title'])){
$pageTitleName = $pageTexts[$pageName.'-title'];
}

if(isset($meta) && isset($pageTexts[$meta])){
$meta = $pageTexts[$meta];
}

?>
<!DOCTYPE html>
<html lang="<?php echo $userLang;?>">
<?php
require('head.php');
?>

<body>
<?php
$facebook->script($siteLang);
require('header.php');
require('nav.php');
?>
