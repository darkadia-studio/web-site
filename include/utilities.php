<?php
	class utilities{
		
		function cryptPassWord($pass){
			return password_hash($pass, PASSWORD_BCRYPT,['cost'=>12]);
		}
		
		function verifyPassWord($pass, $hash){
			return password_verify($pass, $hash);
		}
		
		function strContainArrayValue($string, array $array){
			foreach($array as $value){
				if(strstr(strtolower($string), strtolower($value))){
					return array(true, ucfirst($value));
				}
			}
			return array(false);
		}
		
		function closeHtmlTag($text){
			preg_match_all("/<[^>]*>/", $text, $bal);
			$liste = array();
			foreach($bal[0] as $balise) {
				if ($balise{1} != "/") { // opening tag
					preg_match("/<([a-z]+[0-9]*)/i", $balise, $type);
					// add the tag
					$liste[] = $type[1];
					} else { // closing tag
					preg_match("/<\/([a-z]+[0-9]*)/i", $balise, $type);
					// strip tag
					for ($i=count($liste)-1; $i>=0; $i--){
						if ($liste[$i] == $type[1])
						$liste[$i] = "";
					}
				}
			}
			$tags = '';
			for ($i=count($liste)-1; $i>=0; $i--){
				if ($liste[$i] != "" && ($liste[$i] != "br" && $liste[$i] != "img" && $liste[$i] != "hr")) $tags .= '</'.$liste[$i].'>';
			}
			return($tags);
		}
		
		function showMaxCharNews($content, $maxCar, $pageTexts, $link){
			$max=$maxCar;
			if(strlen($content)>=$max){
				// Met la portion de chaine dans $content
				$content=substr($content,0,$max); 
				// position du dernier espace
				$espace=strrpos($content," "); 
				// test si il ya un espace
				if($espace)
				// si ya 1 espace, coupe de nouveau la chaine
				$content=substr($content,0,$espace);
				
				$str = $content;
				if(preg_match_all("/<\/*([a-zA-Z]+)(?:\s*.*?)>/", $str, $matches, PREG_OFFSET_CAPTURE)) {
					unset($matches[1]);
					$matches = $matches[0];
					for($i=0;$i<count($matches);$i++){
						array_push($matches[$i], ($matches[$i][1]+strlen($matches[$i][0])));
					}
				}
				
				$deletedTag = array('audio','video','source','form');
				$savedTag = array('br','img','hr','h1','h2','h3','h4','h5','h6','ul','ol','li','table','tr','th','td','thead','tbody','tfoot','span','div');
				
				$lastDeletedIndex = -1;
				$firstDeletedIndex = -1;
				
				$lastSavedIndex = -1;
				$firstSavedIndex = -1;
				
				if(!utilities::strContainArrayValue(end($matches)[0],$savedTag)[0]){
					if(substr(end($matches)[0],0,2)!='</'){
						$content = substr($content,0,end($matches)[1]);
						array_pop($matches);
					}
				}
				
				for($i=count($matches)-1;$i>=0;$i--){
					$temp1 = $matches[$i];
					$temp2 = $temp1[0];
					$temp = ($deletedTest = utilities::strContainArrayValue($temp2,$deletedTag));
					if($temp[0]){
						if(substr($temp2,0,2)=='</'){
							$lastDeletedIndex = $temp1[2];
						} else {
							$firstDeletedIndex = $temp1[1];
						}
						if($lastDeletedIndex>-1 && $firstDeletedIndex>-1 && $lastDeletedIndex>$firstDeletedIndex){
							$str1 = substr($content,0,$firstDeletedIndex);
							$str2 = substr($content,$lastDeletedIndex);
							$content = $str1.''.$pageTexts['seeCompleteFor'.$deletedTest[1]].''.$str2;
							$firstDeletedIndex = $lastDeletedIndex = -1;
							} else if($lastDeletedIndex==-1 && $firstDeletedIndex>-1){
							$str1 = substr($content,0,$firstDeletedIndex);
							$str2 = substr($content,$lastDeletedIndex);
							$content = $str1.''.$pageTexts['seeCompleteFor'.$deletedTest[1]].''.$str2;
							$firstDeletedIndex = $lastDeletedIndex = -1;
						}
					}
				}
				// Ajoute ... � la chaine
				$content .= ' <h4>... ';
				$content .=  '<a href="'.$link.'">';
				$content .=  $pageTexts['seeCompleteNews'];
				$content .= '</a></h4>';
				$content .= utilities::closeHtmlTag($content);
			}
			return $content;
		}
		
		function generateConnectionToken($ip) {
			$length = 15;
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$token = '';
			$data = array();
			for ($i = 1; $i <= $length; $i++) {
				if($i%5==0 && $i!=0){
					$token .= '-';
					} else {
					$char = $characters[rand(0, $charactersLength - 1)];
					$token .= $char;
					$data[] = self::text2bin($char);
				}
			}
			$ipArr = explode('.', $ip);
			for($i=0; $i<count($ipArr);$i++){
				$data[] = str_pad(decbin($ipArr[$i]), 8, 0, STR_PAD_LEFT);
			}
			$key = '00000000';
			for($i=0; $i<count($data);$i++){
				$key = str_pad(decbin(bindec($key) ^ bindec($data[$i])), 8, 0, STR_PAD_LEFT);
			}
			$token .= strtoupper(str_pad(base_convert($key,2,16), 2, 0, STR_PAD_LEFT));
			
			return $token;
		}
		
		function verifySecurity($ip, $token) {
			$data = str_split(str_replace('-','',$token));
			$key = $data[count($data)-2].$data[count($data)-1];
			array_pop($data);
			array_pop($data);
			for($i=0; $i<count($data);$i++){
				$data[$i] = self::text2bin($data[$i]);
			}
			$ipArr = explode('.', $ip);
			for($i=0; $i<count($ipArr);$i++){
				$data[] = str_pad(decbin($ipArr[$i]), 8, 0, STR_PAD_LEFT);
			}
			$exactKey = '00000000';
			for($i=0; $i<count($data);$i++){
				$exactKey = str_pad(decbin(bindec($exactKey) ^ bindec($data[$i])), 8, 0, STR_PAD_LEFT);
			}
			
			return $key == strtoupper(str_pad(base_convert($exactKey,2,16), 2, 0, STR_PAD_LEFT));
		}
		
		function getIp(){
			$ip = ($ip = getenv('HTTP_FORWARDED_FOR')) ? $ip :
			($ip = getenv('HTTP_X_FORWARDED_FOR'))     ? $ip :
			($ip = getenv('HTTP_X_COMING_FROM'))       ? $ip :
			($ip = getenv('HTTP_VIA'))                 ? $ip :
			($ip = getenv('HTTP_XROXY_CONNECTION'))    ? $ip :
			($ip = getenv('HTTP_CLIENT_IP'))           ? $ip :
			($ip = getenv('REMOTE_ADDR'))              ? $ip :
			'0.0.0.0';
			return $ip;
		}
		
		function bin2text($bin_str){ 
			$text_str = ''; 
			$chars = EXPLODE("\n", CHUNK_SPLIT(STR_REPLACE("\n", '', $bin_str), 8)); 
			$_I = COUNT($chars); 
			FOR($i = 0; $i < $_I; $text_str .= CHR(BINDEC($chars[$i])), $i++); 
			RETURN $text_str; 
		} 
		
		function text2bin($txt_str){ 
			$len = STRLEN($txt_str); 
			$bin = ''; 
			FOR($i = 0; $i < $len; $i++){ 
				$bin .= STRLEN(DECBIN(ORD($txt_str[$i]))) < 8 ? STR_PAD(DECBIN(ORD($txt_str[$i])), 8, 0, STR_PAD_LEFT) : DECBIN(ORD($txt_str[$i])); 
			} 
			RETURN $bin; 
		} 
		
	}
?>