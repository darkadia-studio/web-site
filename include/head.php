<head>
<script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="/ckeditor/ckeditor.js"></script>

	<?php if($pageName=='signup' || $pageName=='contact'){ ?>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	<?php } ?>

	<link rel="stylesheet" type="text/css" href="/css/style.php">
	<link rel="stylesheet" type="text/css" href="/img/flags/flags.css">
	<link rel="shortcut icon" type="image/ico" href="/img/favicon.gif">
	<title><?php echo $pageTitleName;?></title>


	<?php
		if(isset($meta)){
		?>
		<META NAME='DESCRIPTION' CONTENT='<?php echo $meta?>'>
		<META NAME='KEYWORDS' CONTENT='Evolving World, City Builder, Video Game, Simulation, Construction de ville, Jeu vidéo, Tycoon, Darkadia Studio'>
		<?php
		}
	?>
	<meta name="google-site-verification" content="QWagtnNOElN3jPzyzWUOPJxE-bb5pLKVVqshdPQEkKI" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<META NAME='TITLE' CONTENT='Evolving World'>
	<META NAME='AUTHOR' CONTENT='Nivvdiy'>
	<META NAME='LANGUAGE' CONTENT='<?php
		$langs = $bdd->select('*', 'lang');
		foreach($langs as $index=>$lang){
			echo $lang['acronym'];
			if(isset($langs[$index+1])){
				echo ',';
			}
		}
	?>' /><META NAME='robots' CONTENT='all'>
	<META CONTENT="text/html; charset=utf-8" HTTP-EQUIV="content-type">
	<?php
		echo $facebook->meta();
	?>

</head>