<?php
class authorisation{
	
	static function isEnoughAuth($requireAuth, $auth){
		$requireAuthBits = str_split(str_pad(decbin($requireAuth), 16, '0', STR_PAD_LEFT));
		$authBits = str_split(str_pad(decbin($auth), 16, "0", STR_PAD_LEFT));
		for($i = 0; $i<16; $i++){
			if($requireAuthBits[$i] == 1){
				if($authBits[$i]==0){
					return false;
				}
			}
		}
		return true;
	}
	
}
?>