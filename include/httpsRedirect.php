<?php
	if(!strstr($_SERVER['HTTP_HOST'],'evolvingworld.dev')){
		if(!strstr($_SERVER['HTTP_HOST'],'evolving-world.dev')){
			if(!strstr($_SERVER['HTTP_HOST'],'localhost')){
				if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.') {
					header('Location: https://www.'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
					exit();
				}
				if($_SERVER["HTTPS"] != 'on'){
					header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
					exit();
				}
			}
		}
	}
?>