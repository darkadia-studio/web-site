<?php
    $pageTexts = null;
	//A envoyer sur le script
	//----Token----//
	$pageTexts['token'] = 'Clé de réservation';
	$pageTexts['invalidTokenError'] = 'Clé invalide';
	$pageTexts['noTokenError'] = 'Clé obligatoire';
	$pageTexts['tokenPlaceholder'] = 'Clé de réservation';
	$pageTexts['tokenHelp'] = 'Token Help';
  $pageTexts['innexistantTokenStorage'] = 'Clé inexistante, veuillez nous contacter';
  $pageTexts['invalidEmailTokenInput'] = 'Cette adresse mail n\'est pas associé';
  $pageTexts['noEmailTokenInput'] = 'Adresse email obligatoire';
  $pageTexts['invalidTokenInput'] = 'Mauvaise clé';
  $pageTexts['validToken'] = 'Clé valide';
	//----Username----//
	$pageTexts['reservedUsername'] = 'Pseudo reservé';
	$pageTexts['existingUsername'] = 'Pseudo indisponible';
	$pageTexts['validUsername'] = 'Pseudo valide';
	$pageTexts['usernameHelp'] = 'Username  Help';
	//----Email----//
	$pageTexts['invalidEmailError'] = 'Adresse email invalide';
	$pageTexts['existingEmail'] = 'Adresse email utilisé';
	$pageTexts['noEmailError'] = 'Adresse email obligatoire';
	$pageTexts['validEmail'] = 'Adresse email valide';
	$pageTexts['emailHelp'] = 'Email Help';
	//----Email Check----//
	$pageTexts['invalidEmailCheck'] = 'Adresse email invalide';
	$pageTexts['noSameEMailError'] = 'Les adresses doivent être identique';
	$pageTexts['validEmailCheck'] = 'Les adresses sont identique';
	$pageTexts['emailCheckHelp'] = 'Email check Help';
	//----Password----//
	$pageTexts['noPasswordError'] = 'Mot de passe obligatoire';
	$pageTexts['tooShortPassword'] = 'Mot de passe trop court';
	$pageTexts['tooLongPassword'] = 'Mot de passe trop long';
	$pageTexts['invalidPasswordError'] = 'Mot de passe invalide';
	$pageTexts['validPassword'] = 'Mot de passe valide';
	$pageTexts['passwordHelp'] = 'Password Help';
	//----Password Check----//
	$pageTexts['invalidPasswordCheck'] = 'Mot de passe invalide';
	$pageTexts['noSamePasswordError'] = 'Les mots de passe doivent être identique';
	$pageTexts['validPasswordCheck'] = 'Les mots de passes sont identique';
	$pageTexts['passwordCheckHelp'] = 'Password check Help';
	//----Lastname----//
	$pageTexts['invalidLastname'] = 'Nom invalide';
	$pageTexts['validLastname'] = 'Nom valide';
	$pageTexts['lastNameHelp'] = 'Lastname Help';
	//----Firstname----//
	$pageTexts['invalidFirstname'] = 'Prénom invalide';
	$pageTexts['validFirstname'] = 'Prénom valide';
	$pageTexts['firstNameHelp'] = 'Firstname Help';
	//----Birthday----//
	$pageTexts['birthdayHelp'] = 'Birthday Help';
	//----Buttons----//
	$pageTexts['existingUsernameAccount'] = 'Me connecter';
	$pageTexts['haveAToken'] = 'J\'ai une clé de réservation';
	$pageTexts['myUsername'] = 'C\'est mon pseudo, contacter le staff';
	$pageTexts['lostPassword'] = 'Mot de passe perdu';
	$pageTexts['reset'] = 'Effacer';

//texte directement sur la page
	$pageTexts['connectionData'] = 'Info de connexion';
	$pageTexts['username'] = 'Pseudo';
	$pageTexts['usernamePlaceholder'] = 'Votre pseudo';
	$pageTexts['email'] = 'Email';
	$pageTexts['emailPlaceholder'] = 'Votre email';
	$pageTexts['emailCheck'] = 'Vérification d\'email';
	$pageTexts['emailCheckPlaceholder'] = 'Retapez votre email';
	$pageTexts['password'] = 'Mot de passe';
	$pageTexts['passwordPlaceholder'] = 'Tapez votre mot de passe';
	$pageTexts['passwordCheck'] = 'Vérification de mot de passe';
	$pageTexts['passwordCheckPlaceholder'] = 'Retapez votre mot de passe';
	$pageTexts['personalData'] = 'Info perso';
	$pageTexts['lastname'] = 'Nom de famille';
	$pageTexts['lastnamePlaceholder'] = 'Rentrez votre nom de famille';
	$pageTexts['firstname'] = 'Prénom';
	$pageTexts['firstnamePlaceholder'] = 'Rentrez votre prénom';
	$pageTexts['birthday'] = 'Date de naissance';
	$pageTexts['gender'] = 'Sexe';
	$pageTexts['genderFemale'] = 'Femme';
	$pageTexts['genderMale'] = 'Homme';
	$pageTexts['lang'] = 'Langue';
	$pageTexts['country'] = 'pays';
	$pageTexts['agreeTerms'] = 'J\'ai lu et accepte les conditions d\'utilisations';
	$pageTexts['confirm'] = 'S\'inscrire';
	$pageTexts['termsTitle'] = 'Conditions d\'utilisations';
	$pageTexts['terms'] = '<div id="lipsum">
	<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in tellus blandit, facilisis lacus sit amet, pharetra purus. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed eu ligula sagittis, suscipit orci et, commodo quam. Praesent purus ante, laoreet ut libero in, consectetur facilisis velit. Pellentesque tristique massa urna. Nulla vestibulum, sem at ultricies tempus, orci mi rutrum risus, ut pharetra ligula dui sed felis. Aliquam vehicula elit eros, a iaculis velit posuere hendrerit. Aliquam risus lorem, auctor sed sodales ut, interdum ac nisi. In hac habitasse platea dictumst. Etiam elementum, dui vel faucibus aliquam, mi metus venenatis massa, sed finibus velit ex vitae ante. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
	</p>
	<p>
	Quisque finibus libero ut congue euismod. Nulla a venenatis odio. Nulla facilisi. Donec sit amet enim id augue ultricies fringilla. Ut nibh sem, tristique a lacinia quis, tristique sed nunc. Curabitur tellus quam, accumsan ut commodo vel, posuere sed tortor. Cras quis aliquam tellus, eu placerat neque. Sed vel enim et felis placerat lobortis. Curabitur euismod risus a massa vehicula, sed lacinia magna tincidunt. Pellentesque pellentesque, purus eget condimentum consectetur, urna leo elementum turpis, in lobortis arcu nisi vel massa.
	</p>
	<p>
	Praesent feugiat, eros ac placerat porttitor, ante nibh luctus augue, facilisis lacinia purus dui quis ligula. Etiam a rutrum velit. Maecenas sed dui non mi faucibus sodales sit amet vel elit. Cras pretium et sem pulvinar pellentesque. Cras tempor luctus tortor molestie finibus. Proin scelerisque ex sit amet velit pharetra ultricies. Sed iaculis erat vel ante tempus condimentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
	</p>
	<p>
	Morbi vel augue lorem. Cras sit amet lorem sit amet metus lobortis sagittis consectetur eu sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin ac tellus sit amet nulla faucibus mattis vel id diam. Duis et imperdiet elit. Morbi vel metus eu nisl egestas facilisis. Fusce sem orci, ultrices in tincidunt eu, pulvinar et nisl. Ut finibus id risus eu rutrum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse potenti. Proin sit amet enim pretium, dapibus lectus quis, luctus ante. Aenean nec risus ac tellus viverra blandit.
	</p></div>';

	$pageText2 = translate($pageTexts);
?>
INSERT INTO `text` (`id`, `id_lang`, `pages`, `name`, `trad`) VALUES
<?php foreach($pageTexts as $key => $value){
  echo "(NULL, '1', 'signup', '".$key."', '".$value."'),(NULL, '2', 'signup', '".$key."', '".$pageText2[$key]."')";
  if (next($pageTexts)==true){
      echo ','."\r\n";
  } else {
      echo ';';
  }
}
?>
<?php
function translate($texts){
    $api_key = 'AIzaSyBy6Kbjp0fqUtIAbM36dYzHrW6Ol7jH6GU';
$url = 'https://www.googleapis.com/language/translate/v2?target=en&source=fr&key=' . $api_key;

foreach($texts as $key => $value){
	$url .= '&q='.rawurlencode($value);
	break;
}
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
$response = curl_exec($curl);
curl_close($curl);
var_dump($response);
var_dump($curl);
var_dump($url);
$obj =json_decode($response,true); //true converts stdClass to associative array.
if($obj != null)
{
    if(isset($obj['error']))
    {
        echo "Error is : ".$obj['error']['message'];
    }
    else
    {
        echo "Translsated Text: ".$obj['data']['translations'][0]['translatedText']."n";
    }
}
else
    echo "UNKNOW ERROR";
 
}
?>