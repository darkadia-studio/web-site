<?php
	if(isset($_POST)){
		if(isset($_POST['valueToSearch'])){
			include('../include/bdd.php');
			
			$bdd = new bdd;
			$bdd->connectDB();
			$request = '';
			switch($_POST['valueToSearch']){
				case 'existingEmail':
				$request = 'SELECT EXISTS( SELECT `users`.`email` FROM `users` WHERE `users`.`email` = \''.$_POST['value'].'\' ) AS \'existingValue\'';
				break;
				
				case 'existingUsername':
				$request = 'SELECT EXISTS( SELECT `users`.`username` FROM `users` WHERE `users`.`username` = \''.$_POST['value'].'\' ) AS \'existingValue\'';
				break;
				
				case 'reservedUsername':
				$request = 'SELECT EXISTS( SELECT `reserved_username`.`username` FROM `reserved_username` WHERE `reserved_username`.`username` = \''.$_POST['value'].'\' ) AS \'existingValue\'';
				break;
				
				case 'token':
				$request = 'SELECT `username`, `email`, `token` FROM `reserved_username` WHERE `username` = \''.$_POST['username'].'\'';//Vérifier que le token existe et que le pseudo associé est le bon et que l'adresse mail associé est correcte
				break;
			}
			$data = $bdd->selectSpecialReq($request);
			if($_POST['valueToSearch']!='token'){
				echo $data[0]['existingValue'];
			} else {
				if($data[0]['token']==null){
					echo 0;
				} else if($data[0]['email']!=$_POST['email']){
					echo -1;
				} else if($data[0]['token']!=$_POST['token']){
					echo -2;
				} else {
					echo 1;
				}

			}
		}
	}
?>