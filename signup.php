<?php 
	$meta = 'signin-meta';
	require('include/data.php');
?>

<?php
	//A envoyer sur le script
	//----Token----//
	$pageTexts['token'] = 'Clé de réservation';
	$pageTexts['invalidTokenError'] = 'Clé invalide';
	$pageTexts['noTokenError'] = 'Clé obligatoire';
	$pageTexts['tokenPlaceholder'] = 'Clé de réservation';
	$pageTexts['tokenHelp'] = 'Token Help';
  $pageTexts['innexistantTokenStorage'] = 'Clé inexistante, veuillez nous contacter';
  $pageTexts['invalidEmailTokenInput'] = 'Cette adresse mail n\'est pas associé';
  $pageTexts['noEmailTokenInput'] = 'Adresse email obligatoire';
  $pageTexts['invalidTokenInput'] = 'Mauvaise clé';
  $pageTexts['validToken'] = 'Clé valide';
	//----Username----//
	$pageTexts['reservedUsername'] = 'Pseudo reservé';
	$pageTexts['existingUsername'] = 'Pseudo indisponible';
	$pageTexts['validUsername'] = 'Pseudo valide';
	$pageTexts['usernameHelp'] = 'Username  Help';
	//----Email----//
	$pageTexts['invalidEmailError'] = 'Adresse email invalide';
	$pageTexts['existingEmail'] = 'Adresse email utilisé';
	$pageTexts['noEmailError'] = 'Adresse email obligatoire';
	$pageTexts['validEmail'] = 'Adresse email valide';
	$pageTexts['emailHelp'] = 'Email Help';
	//----Email Check----//
	$pageTexts['invalidEmailCheck'] = 'Adresse email invalide';
	$pageTexts['noSameEMailError'] = 'Les adresses doivent être identique';
	$pageTexts['validEmailCheck'] = 'Les adresses sont identique';
	$pageTexts['emailCheckHelp'] = 'Email check Help';
	//----Password----//
	$pageTexts['noPasswordError'] = 'Mot de passe obligatoire';
	$pageTexts['tooShortPassword'] = 'Mot de passe trop court';
	$pageTexts['tooLongPassword'] = 'Mot de passe trop long';
	$pageTexts['invalidPasswordError'] = 'Mot de passe invalide';
	$pageTexts['validPassword'] = 'Mot de passe valide';
	$pageTexts['passwordHelp'] = 'Password Help';
	//----Password Check----//
	$pageTexts['invalidPasswordCheck'] = 'Mot de passe invalide';
	$pageTexts['noSamePasswordError'] = 'Les mots de passe doivent être identique';
	$pageTexts['validPasswordCheck'] = 'Les mots de passes sont identique';
	$pageTexts['passwordCheckHelp'] = 'Password check Help';
	//----Lastname----//
	$pageTexts['invalidLastname'] = 'Nom invalide';
	$pageTexts['validLastname'] = 'Nom valide';
	$pageTexts['lastNameHelp'] = 'Lastname Help';
	//----Firstname----//
	$pageTexts['invalidFirstname'] = 'Prénom invalide';
	$pageTexts['validFirstname'] = 'Prénom valide';
	$pageTexts['firstNameHelp'] = 'Firstname Help';
	//----Birthday----//
	$pageTexts['birthdayHelp'] = 'Birthday Help';
	//----Buttons----//
	$pageTexts['existingUsernameAccount'] = 'Me connecter';
	$pageTexts['haveAToken'] = 'J\'ai une clé de réservation';
	$pageTexts['myUsername'] = 'C\'est mon pseudo, contacter le staff';
	$pageTexts['lostPassword'] = 'Mot de passe perdu';
	$pageTexts['reset'] = 'Effacer';

//texte directement sur la page
	$pageTexts['connectionData'] = 'Info de connexion';
	$pageTexts['username'] = 'Pseudo';
	$pageTexts['usernamePlaceholder'] = 'Votre pseudo';
	$pageTexts['email'] = 'Email';
	$pageTexts['emailPlaceholder'] = 'Votre email';
	$pageTexts['emailCheck'] = 'Vérification d\'email';
	$pageTexts['emailCheckPlaceholder'] = 'Retapez votre email';
	$pageTexts['password'] = 'Mot de passe';
	$pageTexts['passwordPlaceholder'] = 'Tapez votre mot de passe';
	$pageTexts['passwordCheck'] = 'Vérification de mot de passe';
	$pageTexts['passwordCheckPlaceholder'] = 'Retapez votre mot de passe';
	$pageTexts['personalData'] = 'Info perso';
	$pageTexts['lastname'] = 'Nom de famille';
	$pageTexts['lastnamePlaceholder'] = 'Rentrez votre nom de famille';
	$pageTexts['firstname'] = 'Prénom';
	$pageTexts['firstnamePlaceholder'] = 'Rentrez votre prénom';
	$pageTexts['birthday'] = 'Date de naissance';
	$pageTexts['gender'] = 'Sexe';
	$pageTexts['genderFemale'] = 'Femme';
	$pageTexts['genderMale'] = 'Homme';
	$pageTexts['lang'] = 'Langue';
	$pageTexts['country'] = 'pays';
	$pageTexts['agreeTerms'] = 'J\'ai lu et accepte les conditions d\'utilisations';
	$pageTexts['confirm'] = 'S\'inscrire';
	$pageTexts['termsTitle'] = 'Conditions d\'utilisations';
	$pageTexts['terms'] = '<div id="lipsum">
	<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in tellus blandit, facilisis lacus sit amet, pharetra purus. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed eu ligula sagittis, suscipit orci et, commodo quam. Praesent purus ante, laoreet ut libero in, consectetur facilisis velit. Pellentesque tristique massa urna. Nulla vestibulum, sem at ultricies tempus, orci mi rutrum risus, ut pharetra ligula dui sed felis. Aliquam vehicula elit eros, a iaculis velit posuere hendrerit. Aliquam risus lorem, auctor sed sodales ut, interdum ac nisi. In hac habitasse platea dictumst. Etiam elementum, dui vel faucibus aliquam, mi metus venenatis massa, sed finibus velit ex vitae ante. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
	</p>
	<p>
	Quisque finibus libero ut congue euismod. Nulla a venenatis odio. Nulla facilisi. Donec sit amet enim id augue ultricies fringilla. Ut nibh sem, tristique a lacinia quis, tristique sed nunc. Curabitur tellus quam, accumsan ut commodo vel, posuere sed tortor. Cras quis aliquam tellus, eu placerat neque. Sed vel enim et felis placerat lobortis. Curabitur euismod risus a massa vehicula, sed lacinia magna tincidunt. Pellentesque pellentesque, purus eget condimentum consectetur, urna leo elementum turpis, in lobortis arcu nisi vel massa.
	</p>
	<p>
	Praesent feugiat, eros ac placerat porttitor, ante nibh luctus augue, facilisis lacinia purus dui quis ligula. Etiam a rutrum velit. Maecenas sed dui non mi faucibus sodales sit amet vel elit. Cras pretium et sem pulvinar pellentesque. Cras tempor luctus tortor molestie finibus. Proin scelerisque ex sit amet velit pharetra ultricies. Sed iaculis erat vel ante tempus condimentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
	</p>
	<p>
	Morbi vel augue lorem. Cras sit amet lorem sit amet metus lobortis sagittis consectetur eu sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin ac tellus sit amet nulla faucibus mattis vel id diam. Duis et imperdiet elit. Morbi vel metus eu nisl egestas facilisis. Fusce sem orci, ultrices in tincidunt eu, pulvinar et nisl. Ut finibus id risus eu rutrum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse potenti. Proin sit amet enim pretium, dapibus lectus quis, luctus ante. Aenean nec risus ac tellus viverra blandit.
	</p></div>';
?>
<script src="/javascript/signinCheck.js"></script>

<div class="jumbotron alert alert-danger"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> This page is not functional for the moment, please be patient</div>

<div id="signupTerms">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="signup">
	<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend><?php echo $pageTexts['connectionData'];?></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="username"><?php echo $pageTexts['username'];?> :</label>  
  <div class="col-md-4">
  <input onkeyup="check('username')" onfocus="showHelp('username')" onblur="hideHelp('username')"  id="username" name="username" type="text" placeholder="<?php echo $pageTexts['usernamePlaceholder'];?>" class="form-control input-md" required="">
  <span class="help-block" id="usernameHelp"></span>  
  </div>
  <div class="col-md-4" id="msgBoxUsername"></div>
</div>

<!-- Text input-->
<div class="form-group" id="forToken">
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email"><?php echo $pageTexts['email'];?> :</label>  
  <div class="col-md-4">
  <input onkeyup="check('email')" onfocus="showHelp('email')" onblur="hideHelp('email')" id="email" name="email" type="email" placeholder="<?php echo $pageTexts['emailPlaceholder'];?>" class="form-control input-md" required="">
  <span class="help-block" id="emailHelp"></span>  
  </div>
  <div class="col-md-4" id="msgBoxEmail"></div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="emailCheck"><?php echo $pageTexts['emailCheck'];?> :</label>  
  <div class="col-md-4">
  <input onkeyup="check('emailCheck')" onfocus="showHelp('emailCheck')" onblur="hideHelp('emailCheck')" id="emailCheck" name="emailCheck" type="email" placeholder="<?php echo $pageTexts['emailCheckPlaceholder'];?>" class="form-control input-md" required="">
  <span class="help-block" id="emailCheckHelp"></span>  
  </div>
  <div class="col-md-4" id="msgBoxEmailCheck"></div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="password"><?php echo $pageTexts['password'];?> :</label>
  <div class="col-md-4">
    <input onkeyup="check('password')" onfocus="showHelp('password')" onblur="hideHelp('password')" id="password" name="password" type="password" placeholder="<?php echo $pageTexts['passwordPlaceholder'];?>" class="form-control input-md" required="">
    <span class="help-block" id="passwordHelp"></span>
  </div>
  <div class="col-md-4" id="msgBoxPassword"></div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="passwordCheck"><?php echo $pageTexts['passwordCheck'];?> :</label>
  <div class="col-md-4">
    <input onkeyup="check('passwordCheck')" onfocus="showHelp('passwordCheck')" onblur="hideHelp('passwordCheck')" id="passwordCheck" name="passwordCheck" type="password" placeholder="<?php echo $pageTexts['passwordCheckPlaceholder'];?>" class="form-control input-md" required="">
    <span class="help-block" id="passwordCheckHelp"></span>
  </div>
  <div class="col-md-4" id="msgBoxPasswordCheck"></div>
</div>

<!-- Button (Double) -->
<div class="form-group" id="usernameExistence">
</div>	

</fieldset>
<fieldset>

<!-- Form Name -->
<legend><?php echo $pageTexts['personalData'];?></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="lastname"><?php echo $pageTexts['lastname'];?> :</label>  
  <div class="col-md-4">
  <input onkeyup="check('lastname')" onfocus="showHelp('lastname')" onblur="hideHelp('lastname')"  id="lastname" name="lastname" type="text" placeholder="<?php echo $pageTexts['lastnamePlaceholder'];?>" class="form-control input-md">
  <span class="help-block" id="lastnameHelp"></span>  
  </div>
  <div class="col-md-4" id="msgBoxLastname"></div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="firstname"><?php echo $pageTexts['firstname'];?> :</label>  
  <div class="col-md-4">
  <input onkeyup="check('firstname')" onfocus="showHelp('firstname')" onblur="hideHelp('firstname')"  id="firstname" name="firstname" type="text" placeholder="<?php echo $pageTexts['firstnamePlaceholder'];?>" class="form-control input-md">
  <span class="help-block" id="firstnameHelp"></span>  
  </div>
  <div class="col-md-4" id="msgBoxFirstname"></div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="birthday"><?php echo $pageTexts['birthday'];?> :</label>  
  <div class="col-md-4">
  <input onkeyup="check('birthday')" onfocus="showHelp('birthday')" onblur="hideHelp('birthday')"  id="birthday" name="birthday" type="text" placeholder="" class="form-control input-md">
  <span class="help-block" id="birthdayHelp"></span>  
  </div>
  <div class="col-md-4" id="msgBoxBirthday"></div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="gender"><?php echo $pageTexts['gender'];?> :</label>
  <div class="col-md-8"> 
    <label class="radio-inline" for="gender-0">
      <input type="radio" name="gender" id="gender-0" value="1" checked="checked">
      <?php echo $pageTexts['genderFemale'];?>
    </label> 
    <label class="radio-inline" for="gender-1">
      <input type="radio" name="gender" id="gender-1" value="2">
      <?php echo $pageTexts['genderMale'];?>
    </label>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="country"><?php echo $pageTexts['country'];?> :</label>
  <div class="col-md-4">
    <select id="country" name="country" class="form-control">
		<option value="null"></option>
		<?php
			$request = 'SELECT `countries`.`id`, `countries_names`.`name`'.
				'FROM `countries`'.
				'JOIN `countries_names`'.
				'ON `countries_names`.`id_country` = `countries`.`id`'.
				'JOIN `lang`'.
				'ON `countries_names`.`id_lang` = `lang`.`id`'.
				'WHERE `lang`.`letters` = \''.$userLang.'\''.
				'ORDER BY `countries_names`.`name` ASC';
			$countriesList = $bdd->selectSpecialReq($request);
			foreach($countriesList as $country){
		?>
		<option value="<?php echo $country['id']?>"><?php echo $country['name']?></option>
		<?php
			}
		?>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="lang"><?php echo $pageTexts['lang'];?> :</label>
  <div class="col-md-4">
    <select id="lang" name="lang" class="form-control">
		<option value="null"></option>
		<?php
			$request = 'SELECT `lang`.`id`, `lang_name`.`name`'.
				'FROM `lang_name`'.
				'JOIN `lang`'.
				'ON `lang_name`.`id_lang_translate` = `lang`.`id`'.
				'WHERE `lang`.`letters` = \''.$userLang.'\''.
				'ORDER BY `lang_name`.`name` ASC';
			$langList = $bdd->selectSpecialReq($request);
			foreach($langList as $lang){
		?>
		<option value="<?php echo $lang['id']?>"><?php echo $lang['name']?></option>
		<?php
			}
		?>
    </select>
  </div>
</div>

</fieldset>

<fieldset>

<!-- Form Name -->
<legend></legend>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <div class="col-md-offset-4 col-md-4">
    <label class="checkbox-inline" for="agreeTerms-0">
      <input type="checkbox" name="agreeTerms" id="agreeTerms-0" value="1">
      <?php echo $pageTexts['agreeTerms']?>
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <div class="col-md-offset-3 col-md-4">
	<div class="g-recaptcha" data-sitekey="6LeqqwcUAAAAAA78jxXkZgC175RkwLO3g-SMiJpo"></div>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <div class="col-md-offset-4 col-md-8 btn-group" role="group" >
    <button type="submit"  id="submit" name="submit" class="btn btn-info"><?php echo $pageTexts['confirm']?></button>
    <button type="reset"  id="reset" name="reset" class="btn btn-warning"><?php echo $pageTexts['reset']?></button>
  </div>
</div>	
</fieldset>
	</form>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="terms">
		<h3><?php echo $pageTexts['termsTitle'];?></h3><br>
		<?php echo $pageTexts['terms'];?>	
	</div>
	</div>
<span id="initScript" >
<script type="text/javascript">

function initialize(){
	<?php
		$JSText = array();
		foreach($pageTexts as $key => $value){
		if(strstr(strtolower($key),"token")){
			$JSText[$key] = $value;
		} else if(strstr(strtolower($key),"username")){
			$JSText[$key] = $value;
		} else if(strstr(strtolower($key),"email")){
			$JSText[$key] = $value;
		} else if(strstr(strtolower($key),"password")){
			$JSText[$key] = $value;
		} else if(strstr(strtolower($key),"lastname")){
			$JSText[$key] = $value;
		} else if(strstr(strtolower($key),"firstname")){
			$JSText[$key] = $value;
		} else if(strstr(strtolower($key),"birthday")){
			$JSText[$key] = $value;
		}
		}
	?>
	var sendedTexts = <?php echo json_encode($JSText, JSON_UNESCAPED_UNICODE); ?>;
	initialiseValues(sendedTexts);
}

initialize();
</script>
</span>


<?php
	require('include/footer.php');
?>
</body>
</html>
