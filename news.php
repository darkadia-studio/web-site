<?php 
	$meta = 'news-meta';
	require('include/data.php');
?>

<?php
	$type = null;
	
	if(isset($_GET['type'])){
		$type = $_GET['type'];
	}
	
	if(authorisation::isEnoughAuth(1024, $auth) && $type != 'write'){
		echo '<a id="writeNews" href="/'.$userLang.'/'.$pageName.'/write/">'.$pageTexts['writeNews'].'</a>';
	}
	
	if($type=='write' && authorisation::isEnoughAuth(1024, $auth) ){
		
	?>	
	<form method="POST">
		<textarea name="newsEditor" id="newsEditor" rows="10" cols="80">
			This is my textarea to be replaced with CKEditor.
		</textarea>
		<script>
			CKEDITOR.replace( 'newsEditor', {
				language: '<?php echo $userLang; ?>',
				skin: 'moonocolor'
			});
		</script>
	</form>
	<?php
		} else if($type=='write' && !authorisation::isEnoughAuth(1024, $auth) ){
		
	?>
		<article class="news">
			<h1 class="newsTitle"><?php echo $pageTexts['notEnoughAuthTitle'];?></h1>
			<?php echo $pageTexts['notEnoughAuthText'];?><br>
		</article>
	<?php
		} else if($type!='read'){
		$newsPerPage = 25;
		$newsNumber = $bdd->selectSpecialReq('SELECT COUNT(*) AS \'total\' FROM `news`')[0]['total'];
		
		if($newsNumber>0){
			$pageNumber = ceil($newsNumber/$newsPerPage);
			
			if(isset($_GET['page'])){
				$actualPage=intval($_GET['page']);
				if($actualPage>$pageNumber){
					$actualPage=$pageNumber;
				}
				} else {
				$actualPage=1;
			}
			
			$firstEntry=($actualPage-1)*$newsPerPage;
			
			$authors=array();
			$authorsList = $bdd->selectSpecialReq('SELECT `users`.username AS \'author\''.
			' FROM `users`'.
			' JOIN `news`'.
			' ON `users`.`id` = `news`.`id_author`');
			foreach($authorsList as $author){
				if(!in_array($author['author'], $authors)){
					$authors[] = $author['author'];
				}
			}
			$categories=array();
			$categoriesList = $bdd->selectSpecialReq('SELECT `news_category`.name AS \'category\''.
			' FROM `news_category`'.
			' JOIN `news`'.
			' ON `news_category`.`id` = `news`.`id_category`');
			foreach($categoriesList as $category){
				if(!in_array($category['category'], $categories)){
					$categories[] = $category['category'];
				}
			}
			$years=array();
			$months=array();
			$datesList = $bdd->selectSpecialReq('SELECT `publishedTime` FROM `news` ORDER BY `publishedTime` DESC');
			foreach($datesList as $datel){
				$dates = date_parse(''.$datel['publishedTime']);
				if(!in_array($dates['year'], $years)){
					$years[] = $dates['year'];
				}
				if(!in_array($dates['month'], $months)){
					$months[] = $dates['month'];
				}
			}
			
			$lastsMonths = 11;
			$actualMonth = date('m');
			$actualYear = date('Y');
			echo '<ul class="monthNewsList">';
			for($i=$actualMonth, $urlMonth=$actualMonth, $urlYear = $actualYear; $i>=$actualMonth-$lastsMonths; $i--, $urlMonth--){
				echo '<li class="monthNews">';
				if($urlMonth==0){
					$urlYear--;
					$urlMonth = 12;
				}
				echo '<a href="/'.$userLang.'/'.$pageName.'/'.str_pad($urlMonth,2,0,STR_PAD_LEFT).'-'.$urlYear.'" rel="nofollow">';
				echo $pageTexts['month-'.str_pad($urlMonth,2,0,STR_PAD_LEFT)].' '.$urlYear;
				echo '</a>';
				echo '</li>';
			}
			echo '</ul>';
			
			echo '<ul class="categoriesList">';
			for($i=0; $i<count($categories); $i++){
				echo '<li class="newsCategory">';
				echo '<a href="/'.$userLang.'/'.$pageName.'/cat='.$categories[$i].'" rel="nofollow">';
				echo $categories[$i];
				echo '</a>';
				echo '</li>';
			}
			echo '</ul>';
			
			echo '<ul class="authorsList">';
			for($i=0; $i<count($authors); $i++){
				echo '<li class="newsAuthor">';
				echo '<a href="/'.$userLang.'/'.$pageName.'/aut='.$authors[$i].'" rel="nofollow">';
				echo $authors[$i];
				echo '</a>';
				echo '</li>';
			}
			echo '</ul>';
			
			$cat = 'all';
			$author = 'all';
			$month = 'all';
			$year = 'all';
			$minDate = date('Y-m-d',0);
			$maxDate = date('Y-m-d H:i:s',$date->getTimestamp());
			if(isset($_GET['cat'])){
				$cat = $_GET['cat'];
			}
			if(isset($_GET['author'])){
				$author = $_GET['author'];
			}
			if(isset($_GET['month'])){
				$month = $_GET['month'];
			}
			if(isset($_GET['year'])){
				$year = $_GET['year'];
			}
			if(isset($_GET['minDate'])){
				$minDate = $_GET['minDate'];
			}
			if(isset($_GET['maxDate'])){
				$maxDate = $_GET['maxDate'];
			}
			
			$request = 'SELECT `news`.`name` AS \'newsTitle\', `news`.`id`, `news`.`writedTime`, `news`.`updatedTime`,`news`.`publishedTime`, `users`.`username` AS \'author\', `link-newscat_lang`.`text` as \'category\', `link-news_lang`.`title`,`link-news_lang`.`content`'.
			' FROM `news`'.
			' JOIN `news_category`'.
			' ON `news`.`id_category` = `news_category`.`id`'.
			' JOIN `users`'.
			' ON `news`.`id_author` = `users`.`id`'.
			' JOIN `link-news_lang`'.
			' ON `link-news_lang`.`id_news` = `news`.`id`'.
			' JOIN `link-newscat_lang`'.
			' ON `link-newscat_lang`.`id_category` = `news_category`.`id`'.
			' JOIN `lang`'.
			' ON `link-news_lang`.`id_lang` = `lang`.`id` AND `link-newscat_lang`.`id_lang` = `lang`.`id`'.
			' WHERE `lang`.`letters` = \''.$userLang.'\'';
			if($cat!='all' && $cat != null){
				$request .= ' AND `news_category`.`name` = \''.$cat.'\'';
			}
			if($author!='all' && $author != null){
				$request .= ' AND `users`.`username` = \''.$author.'\'';
			}
			if($month!='all' && $month != null){
				if($year=='all' || $year==null){
					$year = date('Y');
				}
				$request .= 'AND (`news`.`publishedTime` BETWEEN \''.$year.'-'.$month.'-01\' AND \''.$year.'-'.$month.'-31\')';
				} else {
				
			}
			if($minDate != null && $maxDate != null){
				$request .= 'AND (`news`.`publishedTime` BETWEEN \''.$minDate.'\' AND \''.$maxDate.'\')';
			}
			$request .= ' ORDER BY `news`.`publishedTime` DESC LIMIT '.$firstEntry.', '.$newsPerPage;
			
			$newsList = $bdd->selectSpecialReq($request);
			
			if(!empty($newsList)){
				foreach($newsList as $news){
					$news['content']=utilities::showMaxCharNews($news['content'],3000, $pageTexts, ('/'.$userLang.'/'.$pageName.'/read/'.$news['author'].'/'.$news['newsTitle'].'/'.$news['id'].''));
					
					echo '<article class="news">';
					echo '<a href="/'.$userLang.'/'.$pageName.'/read/'.$news['author'].'/'.$news['newsTitle'].'/'.$news['id'].'">';
					echo '<h2 class="newsTitle">'.$news['title'].'</h2>';
					echo '</a>';
					echo '<span class="newsAuthor">'.$pageTexts['writedBy'].' - '.$news['author'].'</span><br>';
					echo $news['content'].'<br>';
					echo '<span class="newsTime">';
					echo $pageTexts['writedOn'].' : '.$news['writedTime'].' ';
					echo $pageTexts['updatedOn'].' : '.$news['updatedTime'].' ';
					echo $pageTexts['publishedOn'].' : '.$news['publishedTime'].' ';
					echo '</span>';
					echo '</article>';
				}
				if($pageNumber>1){
					$passValue = 5;
					$maxShowedPage = 20;
					if($actualPage>1){
						echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-1/'.$pageName.'"><<</a> ';
						echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-'.($actualPage-1).'/'.$pageName.'"><-</a> ';
					}
					for($i=1; $i<=$pageNumber; $i++){
						if($pageNumber<=$maxShowedPage){
							if($i==$actualPage){
								echo ' [ '.$i.' ] '; 
								} else {
								echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-'.$i.'/'.$pageName.'">'.$i.'</a> ';
							}
							} else if($i==1 && $i!=$actualPage){
							echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-'.$i.'/'.$pageName.'">'.$i.'</a> ';
							} else if($i==$pageNumber && $i!=$actualPage){
							echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-'.$i.'/'.$pageName.'">'.$i.'</a> ';
							} else if($i==$actualPage-$passValue){
							echo ' ... ';
							} else if($i==$actualPage+$passValue){
							echo ' ... ';
							} else if($i==$actualPage){
							echo ' [ '.$i.' ] '; 
							} else if($i<$actualPage+$passValue && $i>$actualPage-$passValue){
							echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-'.$i.'/'.$pageName.'">'.$i.'</a> ';
						}
					}
					if($actualPage<$pageNumber){
						echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-'.($actualPage+1).'/'.$pageName.'">-></a> ';
						echo ' <a rel="nofollow" href="'.str_replace('p-'.$actualPage.'/','',str_replace($pageName, '',$_SERVER['REQUEST_URI'])).'p-'.$pageNumber.'/'.$pageName.'">>></a> ';
					}
					echo '</p>';
				}
			}
			} else {
			echo '<article class="news">';
			echo '<h1 class="newsTitle">'.$pageTexts['empty'].'</h1>';
			echo $pageTexts['emptyNews'];
			echo '</article>';
		}
	} else {
		$request = 'SELECT `news`.`name` AS \'newsTitle\', `news`.`id`, `news`.`writedTime`, `news`.`updatedTime`,`news`.`publishedTime`, `users`.`username` AS \'author\', `link-newscat_lang`.`text` as \'category\', `link-news_lang`.`title`,`link-news_lang`.`content`'.
		' FROM `news`'.
		' JOIN `news_category`'.
		' ON `news`.`id_category` = `news_category`.`id`'.
		' JOIN `users`'.
		' ON `news`.`id_author` = `users`.`id`'.
		' JOIN `link-news_lang`'.
		' ON `link-news_lang`.`id_news` = `news`.`id`'.
		' JOIN `link-newscat_lang`'.
		' ON `link-newscat_lang`.`id_category` = `news_category`.`id`'.
		' JOIN `lang`'.
		' ON `link-news_lang`.`id_lang` = `lang`.`id` AND `link-newscat_lang`.`id_lang` = `lang`.`id`'.
		' WHERE `lang`.`letters` = \''.$userLang.'\' AND `users`.`username` = \''.$_GET['author'].'\' AND `news`.`name` = \''.$_GET['title'].'\' AND `news`.`id` = \''.$_GET['id'].'\'';
		
		$news = $bdd->selectSpecialReq($request)[0];
		echo '<article class="news">';
		echo '<h1 class="newsTitle">'.$news['title'].'</h1>';
		echo '<span class="newsAuthor">'.$pageTexts['writedBy'].' - '.$news['author'].'</span><br>';
		echo $news['content'].'<br>';
		echo '<span class="newsTime">';
		echo $pageTexts['writedOn'].' : '.$news['writedTime'].' ';
		echo $pageTexts['updatedOn'].' : '.$news['updatedTime'].' ';
		echo $pageTexts['publishedOn'].' : '.$news['publishedTime'].' ';
		echo '</span>';
		echo '</article>';
	}
?>

<?php
	require('include/footer.php');
?>
</body>
</html>
